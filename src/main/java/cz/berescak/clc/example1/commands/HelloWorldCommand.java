package cz.berescak.clc.example1.commands;

import cz.berescak.clc.commands.Command;
import cz.berescak.clc.commands.ICommand;

@Command(name = "helloWorld",
	description = "Vypisuje 'Ahoj svete'",
	usage = "popis pouziti")
public class HelloWorldCommand implements ICommand{

	public void init() {
		
	}

	public void process() {
		System.out.println("AHOJ SVETE");
	}
	
	

}
