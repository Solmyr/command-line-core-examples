package cz.berescak.clc.example1.commands;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

import cz.berescak.clc.commands.Command;
import cz.berescak.clc.commands.ICommand;

@Command(
		name = "echo",
		description = "Jednoduchy echo prikaz. Opakuje co mu bylo vlozeno v parametrech",
		usage = "popis pouziti")
public class EchoCommand implements ICommand {
	
	@Parameter()
	private List<String> strings = new ArrayList<>();
	
	public void init() {
	
	}

	public void process() {
		for (String string : strings) {
			System.out.print(string + " ");
		}
		System.out.println();
	}

}
