package cz.berescak.clc.example1;

import cz.berescak.clc.CommandLine;

public class Example1 {
	public static void main(String[] args) {
		CommandLine cli = CommandLine.builder()
								.promptSymbol("app$ ")
								.unknownCommandMessage("NEZNAM")
								.exitKeyword("q")
								.helpKeyword("man")
								.addCommandPackage("cz.berescak.clc.example1.commands")
								.build();
		cli.run();
	}
}
